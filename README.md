Hello CI lite
=============
[![Build Status](http://137.226.248.58/api/badges/ywashio/hello-ci/status.svg)](http://137.226.248.58/ywashio/hello-ci)

Let's say hello to CI (represented by drone resp. GitLab Runner), here trimmed trimmed down to the essentials!

# Building

Just
```
make
```

# Dependencies

* make
* g++

# Testing

Just
```
make test
```

# FAQ
## On drone.io
### How to connect your own project with our drone.io instance?

After you've [set up your project](http://readme.drone.io/usage/overview/) (i. e. add a `.drone.yml`) for drone.io,

1. Go to http://137.226.248.58/ (only at ACS)
2. Log in with your [GOGS](http://137.226.248.54/) profile
3. Go to "AVAILABLE REPOSITORIES"
4. Choose the repository you want to activate
5. Click on "ACTIVATE NOW"

### Can I use our drone.io instance for git servers other than our GOGS?

No, that'd require a configuration change in the drone.io server and GOGS works the best with drone.io anyway :)

### Can we install a drone.io instance for the rwth git?

In order to use it with the rwth git, it seems either

* the CI instance has to be publicly reachable (to connect it as a [project service](http://docs.gitlab.com/ce/project_services/project_services.html)) (currently not possible at the ACS) or
* GitLab has to [register the CI instance as an application](http://docs.gitlab.com/ce/integration/oauth_provider.html) with OAuth2 authentication with your account (rwth git does not allow this)

## On GitLab CI
### How to set up a GitLab Runner on our Openstack and connect it with your project at the RWTH GitLab instance?

After you've [set up your project](http://docs.gitlab.com/ce/ci/yaml/README.html) (i. e. add a `.gitlab-ci.yml`) for GitLab CI,

1. Go to our OpenStack Dashboard, go to [Images](http://137.226.248.10/horizon/project/images/), select "Public" and click "Launch Instance" for "gitlab-ci-multi-runner (based on Ubuntu Docker Node)". It is recommended to choose a machine flavor with RAM at least 1024 MB. Refer to the [Cloud Services](http://acswiki.eonerc.rwth-aachen.de/wiki/Cloud+Services) wiki page for more information on setting up your virtual machine in our Open Stack. This sets up an Ubuntu 14.04 environment with the official package gitlab-ci-multi-runner package preinstalled. Then ssh to the instance. (only at ACS)
2. Run `sudo gitlab-ci-multi-runner register` and use the coordinator URL and token from https://git.rwth-aachen.de/your-name/your-project/runners . Currently tested executor is shell or docker with ruby:latest (a stable solution is yet to find). 
3. Refresh the runners page you already opened and see your new runner listed!
4. **Make sure to lock your specific runner from being enabled by other projects** by going to the edit page (click on the :memo: fa-edit icon) of the runner and turning on "Lock to current projects" after turning it on to all the projects you wish to enable the runner to.

### Can I use my GitLab Runner instance on our Open Stack for git servers other than the RWTH GitLab?

Probably yes. Just find out the coordinator URL and token and run the register command again.

## Which docker image to use
### Can I use my custom docker image?

Please upload them at the [Docker Image Library](https://hub.docker.com/).
