EXECUTABLE_NAME=hello-ci

.PHONY: test

all: $(EXECUTABLE_NAME)

hello-ci: main.out
	mv $< $@

%.out: %.cpp
	$(CXX) -o $@ $<

clean:
	-$(RM) *.o

test: $(EXECUTABLE_NAME)
	if [ "`./$(EXECUTABLE_NAME)`" = "Hello, World!" ] ; \
		then echo test succeeded ; true; \
		else echo test failed ; false; \
		fi
